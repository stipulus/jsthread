function init() {
    var thread = new Thread ({
        data: {
            hello:'world'
        },
        run: function (data) {
        	data.msgs = new Array();
        	var Hello = oopi.class({
        		construct: function (msg) {
        			console.log('construct');
        			data.msgs.push(msg);
        		}
        	});

        	postMessage('classes defined');

        	for(var i = 0;i < 20;i++)
        		new Hello('hello');
        },
        callback: function (res) {
            console.log(JSON.stringify(res));
        }
    });
    thread.start();
}